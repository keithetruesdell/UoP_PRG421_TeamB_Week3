package Animal;

import java.util.ArrayList;
import java.util.Iterator;

public class AnimalList {
    private ArrayList<Animal> animalList;

    /***
    * Default Constructor
    */
    public AnimalList() {
        animalList = new ArrayList<Animal>();
    }

    /***
    * adds animal
    * 
    * @param animal
    */
    public void add(Animal animal) {
        animalList.add(animal);
        System.out.println("Animal added successfully to database!");
    }

    /***
    * Removes the animal from the database
    * 
    * @param animal
    */
    public void delete(String animalType) {
        int index = findAnimal(animalType);
        if (index != 1) {
            Animal searchedAnimal = animalList.get(index);
            animalList.remove(searchedAnimal);
            System.out.println("Animal removed from database successfully!");
        } else {
            System.out.println("Animal not foundin database!");
        }
    }

    /**
    * Finds the animal in the database
    * */
    public int findAnimal(String type) {
        
        Iterator i = animalList.iterator();
        
        int pos;
        pos = 0;
        
        while (i.hasNext()) {
            if ( animalList.get(pos).getType().equalsIgnoreCase(type)) {
                return pos;
            }
            
            pos++;
        }
        return -1;
    }

    /**
    * Displays the list of animal's
    * */
    public void displayList() {
        if(animalList.size() == 0){
            System.out.println("Empty List!");
            return;
        }
        
        Iterator i = animalList.iterator();
        
        while (i.hasNext()) {
            System.out.println(i.toString());
        }
        
    }

    /***
    * 
    * @param animal
    */
    public void edit(String animalType, String animalcolor, boolean vertebrate, boolean swim) {
        int index = findAnimal(animalType);
        if (index != -1) {
            Animal searchedAnimal = animalList.get(index);
            searchedAnimal.setColor(animalcolor);
            searchedAnimal.setIsVertebrate(vertebrate);
            searchedAnimal.setCanSwim(swim);

            animalList.set(index, searchedAnimal);
            System.out.println("Animal profile update successfully!");
        } else {
            System.out.println("Animal not found in the database!");
        }
    }
}