package Animal;

import java.util.Scanner;

public class AnimalDriver {
    
    public static AnimalList animalList = new AnimalList();
    
    /**
    * Adds the animal
    * */
    public static void addAnimal(Scanner scanner){
        String type;
        String color;
        boolean vertebrate;
        boolean swim;
        String choiceVertibrate;
        String choiceSwim;
        
        scanner.nextLine();
        System.out.print("What type of Animal: ");
        type = scanner.nextLine();
        System.out.print("Enter the color of Animal: ");
        color = scanner.nextLine();
        System.out.print("Is Animal vertebrate(yes/no): ");
        choiceVertibrate = scanner.nextLine();
        System.out.print("Can Animal swim(yes/no): ");
        choiceSwim = scanner.nextLine();
        
        vertebrate = choiceVertibrate.toLowerCase().equals("y");
        swim = choiceSwim.toLowerCase().equals("y");
        
        // create animal object
        Animal animal = new Animal(type, color, vertebrate, swim);
        
        animalList.add(animal);
    }
    
    /**
    * This method is used to edit an animal
    * */
    public static  void editAnimal(Scanner scanner){
        String animalType;
        String color;
        boolean vertebrate;
        boolean swim;
        String choiceVertibrate;
        String choiceSwim;
        
        scanner.nextLine();
        System.out.print("Enter the type of Animal to edit: ");
        animalType = scanner.nextLine();
        System.out.print("Enter new color of Animal: ");
        color = scanner.nextLine();
        System.out.print("Is Animal vertebrate(yes/no): ");
        choiceVertibrate = scanner.nextLine();
        System.out.print("Can Animal swim(yes/no): ");
        choiceSwim = scanner.nextLine();
        
        vertebrate = choiceVertibrate.toLowerCase().equals("y");
        swim = choiceSwim.toLowerCase().equals("y");
        
        animalList.edit(animalType, color, vertebrate, swim);
    }
    
    /**
    * This method is used to delete an animal
    * */
    public static void deleteAnimal(Scanner scanner){
        String animalType;
        scanner.nextLine();
        System.out.println("Enter the type of Animal to delete: ");
        animalType = scanner.nextLine();
        
        animalList.delete(animalType); 
    }
    
    /**
    * Displays the User Menu
    * */
    public static int userMenu(Scanner scanner) {
        
        System.out.print("\nWelcome to the Animal Database");
        
        System.out.println("\n1. Add a new animal profile");
        System.out.println("2. Edit an animal profile");
        System.out.println("3. Delete an animal's profile");
        System.out.println("4. View an animal's information");
        System.out.println("5. Exit the program");
        
        System.out.print("\nChoose your menu option [1-5]:"); 
        
        int choice = scanner.nextInt();
        return choice;
    }

    /* *
    * Main Menu
    * */
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int choice;

        do {
            choice = userMenu(scanner);
            switch (choice) {
            case 1:
                addAnimal(scanner);
                break;
            case 2:
                editAnimal(scanner);
                break;
            case 3:
                deleteAnimal(scanner);
                break;
            case 4:
                animalList.displayList();
                break;
            case 5:
                System.exit(0);
            }
        } while (choice != 0);
    }
}