package Animal;

/**
 *------------------------------------------------------------------------------
 * @title       Team B - Program Improvement II
 * @week        Week 3
 * @author      Cedrick Drafton, Keith Truesdell
 * @school      University of Phoenix - Online
 * @course      PRG/421 - Java Programming II
 * @instructor  Edward Spear
 * @duedate     11/21/2016
 *------------------------------------------------------------------------------
 * Write a Java program (non-GUI preferred) to demonstrate the use of Iterator.
 * 
 * The program should allow a user to do the following:
 *      Allow the reading of a collection of animal objects from an external file.
 *      Output on screen the content of a collection of animal objects.
 *      Use Iterator to achieve these goals.
 * 
 * Include a brief documentation (in the code or in a separate document) 
 * to explain the input (if any), processing and output of the program
 *------------------------------------------------------------------------------
 * DOCUMENTATION
 * INPUT - PROCESSING - OUTPUT
 * 
 * 
 *------------------------------------------------------------------------------
 */

public class Animal {
    String type; // decribes what type of animal 
    String color; // describes the color of the animal
    boolean isVertebrate; // vertebrate?
    boolean canSwim; // explains if theanimal canswim
    
    /**
    * Constructor
    */
    public Animal(String type, String color, boolean vertebrate, boolean canSwim) {
        this.type = type;
        this.color = color;
        this.isVertebrate = vertebrate;
        this.canSwim = canSwim;
    }

    /**
    * the type of animal will be displayed 
    */
    public String getType() {
        return this.type;
    }

    /**
    * the color of he animal will be displayed
    */
    public String getColor() {
        return this.color;
    }

    /**
    * will display if the animal is a vertebrate
    */
    public boolean isVertebrate() {
        return this.isVertebrate;
    }

    /**
    * will display if the animal can swim
    */
    public boolean canSwim() {
        return this.canSwim;
    }

    /**
    * The type of animal will be selected 
    */
    public void setType(String type) {
        this.type = type;
    }

    /**
    * The color of the animal will be selected
    */
    public void setColor(String color) {
        this.color = color;
    }

    /**
    * Will determine if the animal is a vertebrate
    */
    public void setIsVertebrate(boolean vertebrate) {
        this.isVertebrate = vertebrate;
    }

    /**
    * will determine if the animal can swim
    */
    public void setCanSwim(boolean swim) {
        this.canSwim = swim;
    }

    /**
    * Returns Animal data as string
    * */
    public String toString() {
        return "Type: " + type + ", Color: " + color + ", " + (isVertebrate == true? "is a ": "not a") 
            + " vertebrate, " + (canSwim == true? "can swim": "cannot swim.");

    }
}